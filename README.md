# **_Vitis Vinifera_ Greek accessions**

---

This repository contains scipts for the paper:

Genealogical analyses of three cultivated and one wild specimen of _Vitis vinifera_ from Greece

Authors: Rachita Srivastava1*, Christos Bazakos1,2*, Maroussa Tsachaki3, Kriton Kalantidis4,5, Miltos Tsiantis1, Stefan Laurent1

---

## **Folders** 

### **SNP_calling**
Contains snakemake for SNP calling using gatk

### **Population_Structure**
Contains scripts for PCA and admixture analysis

### **Fst_calculation**
Contains script for Fst calculation for various populations

### **ARG_approach**
Contains scripts for tree sequence inference and gnn analysis

### **Sex_determination**
Contains script for genotype calling for the sex-determination region
