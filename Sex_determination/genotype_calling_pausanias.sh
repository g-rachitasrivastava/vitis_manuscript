bwa mem -t 100 -R '@RG\tID:4157_A_run611_CCGCATAA_S48_L003_idx\tSM:4157_A_run611_CCGCATAA_S48_L003\tLB:4157_A_run611_CCGCATAA_S48_L003_lib\tPL:ILLUMINA' /srv/netscratch/dep_tsiantis/grp_laurent/srivastava/Vitis_Vinifera/Reference/CabSauv_chr2.fa 4157_A_run611_CCGCATAA_S48_L003_R1_001.fastq 4157_A_run611_CCGCATAA_S48_L003_R2_001.fastq | samtools sort -@ 100 -o Paf_map_CabSauv_rg.bam -

bcftools call --threads 7 -mv -Oz -o Pafsanias_chr2.vcf.gz Paf_map_CabSauv_rg.bam
