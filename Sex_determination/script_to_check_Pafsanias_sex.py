import re
import sys

male=open('male','r')
Paf_snps=open('Pafsanias_chr2_snps','r')

out=open('Paf_male','w')
out1=open('Paf_male_diff','w')

s1=list()
s2=list()

for line in male:
    items1=line.strip('\n')
    items=re.split('\t|\n',items1)
    s1.append(items)

for line in Paf_snps:
    items1=line.strip('\n')
    items=re.split('\t|\n',items1)
    s2.append(items)

for i in range(len(s1)):
    for j in range(len(s2)):
        if s1[i][0]==s2[j][0]:
            if s1[i][1]==s2[j][1]:
                out.write(s1[i][0]+"\t"+s1[i][1]+"\t"+s2[j][2]+"\n")
            if s1[i][1]!=s2[j][1]:
                out1.write(s1[i][0]+"\t"+s2[j][1]+"\t"+s2[j][2]+"\n")
                break
