#/srv/netscratch/dep_tsiantis/grp_laurent/srivastava/plink2 --vcf gatk_77_snps_filtered_snps_lessmiss_noWEAWNA.recode.vcf --geno 0.1 --set-all-var-ids @:# --indep-pairwise 100 'kb' 0.2 --threads 6 --out prune --allow-extra-chr
#/srv/netscratch/dep_tsiantis/grp_laurent/srivastava/plink2 --vcf gatk_77_snps_filtered_snps_lessmiss_noWEAWNA.recode.vcf --set-all-var-ids @:# --extract prune.prune.in --make-bed --out snps_filtered_pruned --threads 6 --allow-extra-chr --max-alleles 2

/srv/netscratch/dep_tsiantis/grp_laurent/srivastava/plink2 --vcf concat_snps_less_miss.vcf.gz --geno 0.2 --set-all-var-ids @:# --indep-pairwise 100 'kb' 0.2 --threads 6 --out prune1 --allow-extra-chr
/srv/netscratch/dep_tsiantis/grp_laurent/srivastava/plink2 --vcf concat_snps_less_miss.vcf.gz --set-all-var-ids @:# --extract prune1.prune.in --make-bed --out snps_filtered_pruned_all --threads 6 --allow-extra-chr --max-alleles 2

#/srv/netscratch/dep_tsiantis/grp_laurent/srivastava/plink2 --vcf concat_snps_less_miss_noWNAWEA.recode.vcf.gz --geno 0.2 --set-all-var-ids @:# --indep-pairwise 100 'kb' 0.2 --threads 6 --out prune2 --allow-extra-chr --memory 30000
#/srv/netscratch/dep_tsiantis/grp_laurent/srivastava/plink2 --vcf concat_snps_less_miss_noWNAWEA.recode.vcf.gz --set-all-var-ids @:# --extract prune2.prune.in --make-bed --out snps_filtered_pruned_57 --threads 6 --allow-extra-chr --max-alleles 2 --memory 30000
