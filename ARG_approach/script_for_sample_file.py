import re
import sys
import vcf 
import tsinfer

chromosomes=list(range(1,19))

s1,s2,s3,s4=([] for i in range(4))

file1=open('lat_long_tsinfer','r')
file2=open('cultivars','r')

for line in file1:
    items1=line.strip('\n')
    items=re.split('\t|\n',items1)
    s1.append(items)
    
for line in file2:
    items1=line.strip('\n')
    items=re.split('\t|\n',items1)
    s2.append(items)	


for i in range(len(chromosomes)):
    string="chr"+str(chromosomes[i])+"_phased.vcf"
    string1="chr"+str(chromosomes[i])+"_data.samples"
    vcf_reader=vcf.Reader(open(string,'r'))

    sample_data=tsinfer.SampleData(path=string1)
    sample_data.add_population(metadata={"name":"Israel"})
    sample_data.add_population(metadata={"name":"Asia"})
    sample_data.add_population(metadata={"name":"Mediterranean"})
    sample_data.add_population(metadata={"name":"WEU"})
    sample_data.add_population(metadata={"name":"French"})
    sample_data.add_population(metadata={"name":"Unassigned"})

    for j in range(len(vcf_reader.samples)):
        for k in range(len(s1)):
            if vcf_reader.samples[j]==str(s1[k][0]):
            	if s1[k][8]=="1":
                	sample_data.add_individual(ploidy=2, population=0, metadata={"name":str(s2[k])})
                	break
            if vcf_reader.samples[j]==str(s1[k][0]):
            	if s1[k][8]=="2":
                	sample_data.add_individual(ploidy=2, population=1, metadata={"name":str(s2[k])})
                	break
            if vcf_reader.samples[j]==str(s1[k][0]):
            	if s1[k][8]=="3":
                	sample_data.add_individual(ploidy=2, population=2, metadata={"name":str(s2[k])})
                	break
            if vcf_reader.samples[j]==str(s1[k][0]):
            	if s1[k][8]=="4":
                	sample_data.add_individual(ploidy=2, population=3, metadata={"name":str(s2[k])})
                	break
            if vcf_reader.samples[j]==str(s1[k][0]):
            	if s1[k][8]=="5":
                	sample_data.add_individual(ploidy=2, population=4, metadata={"name":str(s2[k])})
                	break
            if vcf_reader.samples[j]==str(s1[k][0]):
            	if s1[k][8]=="Unassigned":
                	sample_data.add_individual(ploidy=2, population=5, metadata={"name":str(s2[k])})
                	break
                
    for record in vcf_reader:
        pos=record.POS
        genos=list()
        als=list()
        als.append(str(record.REF))
        alt=str(record.ALT).strip('[')
        alt1=alt.strip(']')
        als.append(str(alt1))
        for sample in record.samples:
            genots=str(sample['GT']).split('|')
            genos.append(genots[0])
            genos.append(genots[1])
        sample_data.add_site(pos,genos,als)

    sample_data.finalise()
