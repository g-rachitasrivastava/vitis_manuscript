import re
import tskit
import sys
import pandas as pd
import scipy 
from scipy import stats
from scipy import cluster
from statistics import mean

s1=list()
s2=list()

file1=open('cultivars_114','r')
file2=open('vitis_chr_len','r')

for line in file1:
    items1=line.strip('\n')
    items=re.split('\n',items1)
    s1.append(items)

for line in file2:
    items1=line.strip('\n')
    items=re.split('\t|\n',items1)
    s2.append(items)

greek_cultivars=[0]

indicator_function=dict()

#out=open('weighted_gnn','w')

num_samples=114

for b in range(0,8):
    cnt=0
#    indicator_function=dict()
#    for l in range(0,num_samples):
#        indicator_function[l]=list()
#    string='weighted_gnn_'+str(i)
#    out=open(string,'w')

    for i in range(1,20):
        input_file="chr"+str(i)+"_inferred_tree"
        ts=tskit.load(input_file)
        indicator_function=dict()
        for l in range(0,num_samples):
            indicator_function[l]=list()
        string='weighted_gnn_'+str(b)+"_"+str(i)
        out=open(string,'w')

        for tree in ts.trees():
            cnt=cnt+1
            children=list()
            parent_node=tree.parent(int(b))
            for a in tree.leaves(parent_node):
                children.append(a)
            children.remove(int(b))
            for k in range(0,ts.num_samples):
                if k in children:
                    val=1
                    value=1*(int(tree.span)/int(s2[i][1]))
                    indicator_function[k].append(value)
                else:
                    indicator_function[k].append(0)

        for m in range(0,len(indicator_function)):
            out.write(str(s1[b][0])+str(b)+"\t"+str(s1[m][0])+str(m)+"\t"+str(sum(indicator_function[m]))+"\n")
        out.close()

#with open("indicator_values", 'w') as f: 
#    for key, value in indicator_function.items(): 
#        f.write('%s:%s\n' % (key, value))
