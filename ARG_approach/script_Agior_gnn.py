import tskit
import pandas as pd
import scipy 
from scipy import stats
from scipy import cluster
import re
import sys
from statistics import mean
#import seaborn as sns

s1=list()

file1=open('cultivars03','r')

for line in file1:
    items1=line.strip('\n')
    items=re.split('\n',items1)
    s1.append(items)

print(s1)

for i in range(1,20):
    input_file="chr"+str(i)+"_inferred_tree"
    out=open("gnn_matrix_agiorchr"+str(i),'w')
    ts=tskit.load(input_file)

    Pinot_1=dict()
    Pinot_2=dict()
    WEU_1=dict()
    WEU_2=dict()
    
#76 and 77 is the node number for Chardonnay

#Get the gnn matrix for both haplotypes of chardonnay against all accessions except chardonnay itself (matrix stored for each chromosome in a separate file)

    gnn1=ts.genealogical_nearest_neighbours([4,5],[[0],[1],[2],[3],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[28],[29],[30],[31],[32],[33],[34],[35],[36],[37],[40],[41],[42],[43],[44],[45],[46],[47],[48],[49],[50],[51],[52],[53],[54],[55],[56],[57],[58],[59],[60],[61],[62],[63],[64],[65],[66],[67],[68],[69],[70],[71],[74],[75],[76],[77],[78],[79],[80],[81],[82],[83],[84],[85],[86],[87],[88],[89],[90],[91],[92],[93],[112],[113]],num_threads=7)
    gnn_list=list()
    gnn_list=gnn1.tolist()
    print(len(gnn_list[0]))
    print(len(s1))
    for l in range(0,(len(s1)-1)):
        out.write(str(s1[l][0])+"_1"+"\t")
        out.write(str(s1[l][0])+"_2"+"\t")
    out.write(str(s1[len(s1)-1][0])+"_1"+"\t")
    out.write(str(s1[len(s1)-1][0])+"_2"+"\n")
    for j in range(len(gnn_list)):
        for k in range(0,85):
            out.write(str(gnn_list[j][k])+"\t")
        out.write(str(gnn_list[j][85])+"\n")
