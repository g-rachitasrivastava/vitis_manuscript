# STEPS TO FOLLOW #

### To generate sample data file from vcf file for tsinfer ###
1. ``` python script_for_sample_file.py ```

### To infer tree sequence using tsinfer in snakemake ###
2. ``` snakemake -p -j 19```

### To get genealogical nearest neighbours for Agiorgitiko ###
3. ``` python script_Agior_gnn.py ```

### To avg gnn over all chromosomes for Agiorgitiko ###
4. ``` python script_to_avg_gnn_agior.py ```

### To get genealogical nearest neighbours for Mavrotragano ###
5. ``` python script_Mavro_gnn.py ```

### To avg gnn over all chromosomes for Mavrotragano ###
6. ``` python script_to_avg_gnn_mavro.py ```

### To get genealogical nearest neighbours for Pausanias ###
7. ``` python script_Paf_gnn.py ```

### To avg gnn over all chromosomes for Pausanias ###
8. ``` python script_to_avg_gnn_paf.py ```

### To get genealogical nearest neighbours for Xinomavro ###
9. ``` python script_Xino_gnn.py ```

### To avg gnn over all chromosomes for Xinomavro ###
10. ``` python script_to_avg_gnn_xino.py ```
